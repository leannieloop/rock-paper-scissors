package com.sg.Summative;

import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println(playGame());
    }

    private static String playGame() {
        System.out.println("Let's play Rock Paper Scissors!");
        int numberOfRounds = getNumberOfRounds();
        if (!isValidNumberOfRounds(numberOfRounds)) {
            return "You may only play between 1 and 10 rounds.";
        }

        int ties = 0;
        int userWins = 0;
        int computerWins = 0;
        for (int round = 0; round < numberOfRounds; round++) {
            int userChoice = getUserChoice();
            int computerChoice = getComputerChoice();
            if (isTie(userChoice, computerChoice)) {
                ties++;
                System.out.println("It's a tie!");
            } else if (isUserWin(userChoice, computerChoice)) {
                userWins++;
                System.out.println("You win!");
            } else {
                computerWins++;
                System.out.println("The computer wins!");
            }
            System.out.println();
        }
        printFinalScore(ties, userWins, computerWins);
        shouldStartAnotherGame();
        return "Thanks for playing!";
    }

    private static int getNumberOfRounds() {
        System.out.print("How many rounds would you like to play? (1 - 10) ");
        String numberOfRounds = scanner.nextLine();
        return Integer.parseInt(numberOfRounds);
    }

    public static boolean isValidNumberOfRounds(int numberOfRounds) {
        return numberOfRounds >= 1 && numberOfRounds <= 10;
    }

    private static int getUserChoice() {
        int userChoiceInt;
        do {
            System.out.println("Enter your selection:\n1) Rock\n2) Paper\n3) Scissors");
            String userChoice = scanner.nextLine();
            userChoiceInt = Integer.parseInt(userChoice);
        } while (!isUserChoiceValid(userChoiceInt));
        return userChoiceInt;
    }

    public static boolean isUserChoiceValid(int userChoice) {
        return userChoice >= 1 && userChoice <= 3;
    }

    private static int getComputerChoice() {
        Random random = new Random();
        return random.nextInt(3) + 1;
    }

    public static boolean isTie(int userChoice, int computerChoice) {
        return userChoice == computerChoice;
    }

    public static boolean isUserWin(int userChoice, int computerChoice) {
        return ((userChoice == 2 && computerChoice == 1) || (userChoice == 3 && computerChoice == 2) || (userChoice == 1 && computerChoice == 3));
    }

    private static void printFinalScore(int ties, int userWins, int computerWins) {
        System.out.println("Number of Ties: " + ties);
        System.out.println("Number of User Wins: " + userWins);
        System.out.println("Number of Computer Wins: " + computerWins);
        if (userWins > computerWins && userWins > ties) {
            System.out.println("You won the most rounds!");
        } else if (computerWins > userWins && computerWins > ties) {
            System.out.println("The computer won the most rounds!");
        }
        System.out.println();
    }

    private static void shouldStartAnotherGame() {
        if (askUserToPlayAgain()) {
            playGame();
        }
    }

    private static boolean askUserToPlayAgain() {
        String startAnotherGame;
        boolean userSaysYes, userSaysNo;
        do {
            System.out.print("Would you like to play again? (y/n) ");
            startAnotherGame = scanner.nextLine();
            userSaysYes = startAnotherGame.equalsIgnoreCase("y");
            userSaysNo = startAnotherGame.equalsIgnoreCase("n");
        } while (!userSaysYes && !userSaysNo);
        return userSaysYes;
    }
}
